local battery_widget = require("repo.battery-widget")
local BAT0 = battery_widget { adapter = "BAT0", ac = "AC" }

local battery = {
    widget = BAT0
}

return battery


