local awful = require("awful")

local module = {}

module.toggle_tag = function(i)
    return function()
        local screen = awful.screen.focused()
        local tag = screen.tags[i]
        if tag then
            awful.tag.viewtoggle(tag)
        end
    end
end

module.move_to_tag = function(client)
    return function()
        if client.focus then
            local tag = client.focus.screen.tags[i]
            if tag then
                client.focus:move_to_tag(tag)
            end
        end
    end
end

return module
