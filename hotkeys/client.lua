local awful = require("awful")

local module = {}

module.next_client = function()
    awful.client.focus.byidx(1)
end

module.prev_client = function()
    awful.client.focus.byidx(-1)
end

module.swap_next = function()
    awful.client.swap.byidx(1)
end

module.swap_prev = function()
    awful.client.swap.byidx(-1)
end

module.go_back = function(client)
    return function()
        awful.client.focus.history.previous()
        if client.focus then
            client.focus:raise()
        end
    end
end

module.restore_minimized = function(client)
    return function()
        local c = awful.client.restore()
        -- Focus restored client
        if c then
            client.focus = c
            c:raise()
        end
    end
end

module.full_screen =  function(c)
    c.fullscreen = not c.fullscreen
    c:raise()
end

module.toggle_floating = awful.client.floating.toggle

module.move_to_master = function(c)
    c:swap(awful.client.getmaster())
end

module.move_to_screen = function(c)
    c:move_to_screen()
end

module.keep_on_top = function(c)
    c.ontop = not c.ontop
end

module.maximize = function(c)
    c.maximized = not c.maximized
    c:raise()
end

module.minimize = function(c)
    -- The client currently has the input focus, so it cannot be
    -- minimized, since minimized clients can't have the focus.
    c.minimized = true
end

module.maximize_v = function(c)
    c.maximized_vertical = not c.maximized_vertical
    c:raise()
end

module.maximize_h = function(c)
    c.maximized_horizontal = not c.maximized_horizontal
    c:raise()
end

module.kill = function(c)
    c:kill()
end

return module
