local wibox = require("wibox")

local clock = {
    widget = wibox.widget.textclock("%a %d, %H:%M")
}

return clock
