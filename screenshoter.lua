local awful = require("awful")

local run_spectacle = function()
    awful.spawn("spectacle")
end


local screen = {}

screen.key_shot = awful.key({}, "Print", run_spectacle)

return screen
