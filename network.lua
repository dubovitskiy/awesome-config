local awful = require("awful")

local network = {
    start = function()
        awful.util.spawn("nm-applet &")
    end
}

return network
