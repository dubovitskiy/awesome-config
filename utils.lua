local utils = {}

utils.set_libinput_prop = function(hardware_id, status)
    return function(property)
        return "xinput set-prop " .. hardware_id .. " 'libinput ".. property .." Enabled' " .. status
    end
end

return utils
