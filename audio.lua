local APW = require("repo.apw.widget")
local awful = require("awful")

local audio = {}

audio.widget = APW
audio.key_increase = awful.key({ }, "XF86AudioRaiseVolume",  APW.Up)
audio.key_decrease = awful.key({ }, "XF86AudioLowerVolume",  APW.Down)
audio.key_mute     = awful.key({ }, "XF86AudioMute",         APW.ToggleMute)

return audio
