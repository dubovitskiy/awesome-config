local wibox = require("wibox")

local tray = {
    widget = wibox.widget.systray()
}

return tray
