#!/bin/bash
# Needs xorg-x11-server-utils package to be installed.
# Makes use of `xrandr` command.

set -x

readonly internal="eDP1"
readonly external="HDMI-1"
readonly screen_h="1368"
readonly screen_v="768"
readonly mode="${screen_h}x${screen_v}_60.00"

# Check if mode created
xrandr | grep "${mode}" &> /dev/null
readonly is_mode_created="${?}"

# Create new mode if it has not been created yet
if [[ "${is_mode_created}" == "1" ]]; then

    # Generate mode line
    readonly modeline="$(cvt "${screen_h}" ${screen_v} | tail -n 1 | sed s'/Modeline //' | sed s'/\"//g')"

    # Create new screen resolution
    xrandr --newmode ${modeline} &> /dev/null

    # Add new screen resolution to the internal
    # laptop monitor
    xrandr --addmode "${internal}" "${mode}"
fi

# Change laptop mode to 1368x768
xrandr --output "${internal}" --mode "${mode}"

# Check if an external monitor is connected
xrandr | grep "${external} connected"
readonly is_external_monitor_connected="${?}"

if [[ "${is_external_monitor_connected}" == "0" ]]; then
    # External monitor connected. Just place it automatically
    # above the internal laptop screen.
    xrandr --output "${external}" --auto --above "${internal}"
else
    # External monitor is not connected, so just disable the output
    xrandr --output "${external}" --off
fi

# Set laptop screen as a primary output
xrandr --output "${internal}" --primary
