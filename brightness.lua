-- Screen brightness management module
local awful = require('awful')

local monitor_id = "eDP1" -- may change on different OSes
local COMMAND_GET_CURRENT = "xrandr --verbose | grep -m 1 -i brightness | cut -f2 -d ' '"
local COMMAND_SET_BRIGHTNESS = "xrandr --output " .. monitor_id .. " --brightness "

-- Class module itself
local Bright = {}
Bright.__index = Bright

function Bright.create()
    local self = setmetatable({}, Bright)
    self.current = self._get_current()
    self.step = 0.1
    self.minimal_brightness = 0.1
    return self
end

function Bright._get_current()
    local process = io.popen(COMMAND_GET_CURRENT)
    local result = process:read("*all")
    return tonumber(result)
end

function Bright._set_brigtness(brightness)
    io.popen(COMMAND_SET_BRIGHTNESS .. brightness)
end

function Bright:increase()
    local brightness = self.current + self.step

    if brightness > 1 then
        return false
    end

    self._set_brigtness(brightness)
    self.current = brightness
end

function Bright:decrease()
    local brightness = self.current - self.step

    if brightness < self.minimal_brightness then
        return false
    end

    self._set_brigtness(brightness)
    self.current = brightness
end

local _bright = Bright.create()

local increase_brightness = function()
    _bright:increase()
end

local decrease_brightness = function()
    _bright:decrease()
end

-- Exported helper module
local bright = {
    increase_brightness,
    decrease_brightness,

    key_increase = awful.key({}, "XF86MonBrightnessUp", increase_brightness),
    key_decrease = awful.key({}, "XF86MonBrightnessDown", decrease_brightness),
}
return bright
