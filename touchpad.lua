-- Wrapper module that defines api which needs
-- to be implemented for different hardware

local touchpad = {}

-- to be parsed from config
local touchpad_module_name = 'hardware.dell-touchpad'
local touchpad_module = require(touchpad_module_name)


touchpad.enable_natural_scroll = function()
    touchpad_module.enable_natural_scroll()
end

touchpad.enable_tap_to_click = function()
    touchpad_module.enable_tap_to_click()
end

-- other methods can be defined here

return touchpad
