-- Touchpad interface implementation
local awful = require("awful")
local touchpad = {}

-- to be configured
local TOUCHPAD_ID = "11"

local set_libinput_prop = function(hardware_id, status)
    return function(property)
        return "xinput set-prop " .. hardware_id .. " 'libinput ".. property .." Enabled' " .. status
    end
end

local enable = set_libinput_prop(TOUCHPAD_ID, 1)

touchpad.enable_natural_scroll = function()
    awful.util.spawn_with_shell(enable("Natural Scrolling"))
end

touchpad.enable_tap_to_click = function()
    awful.util.spawn_with_shell(enable("Tapping"))
end

return touchpad
