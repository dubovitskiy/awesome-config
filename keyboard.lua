local awful = require("awful")
local keyboard_layout = require("repo.keyboard_layout")
local kbdcfg = keyboard_layout.kbdcfg({type = "tui"})

kbdcfg.add_primary_layout("English", "US", "us")
kbdcfg.add_primary_layout("Русский", "RU", "ru")

kbdcfg.bind()

kbdcfg.widget:buttons(
    awful.util.table.join(
        awful.button({ }, 1, function () kbdcfg.switch_next() end),
        awful.button({ }, 3, function () kbdcfg.menu:toggle() end)
    )
)

local next_layout = function ()
    kbdcfg.switch_next()
end

local keyboard = {}

keyboard.key_change = awful.key({"Mod4"}, "space", next_layout)
keyboard.widget = kbdcfg.widget

return keyboard
